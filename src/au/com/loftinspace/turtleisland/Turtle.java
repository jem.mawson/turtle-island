package au.com.loftinspace.turtleisland;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * The Turtle class models the turtle drawing object.
 */
public class Turtle {

    /* ------------------------------------------------
       Static constants
       ------------------------------------------------ */

    /* specifies how many degrees there are in a circle */
    private static final double DEGREES_IN_CIRCLE = 360.0;

    /* lists the acceptable colours for the Turtle's pen */
    private static final Color[] COLOUR_LIST = { Color.black, Color.blue,
        Color.cyan, Color.darkGray, Color.gray, Color.green, Color.lightGray,
        Color.magenta, Color.orange, Color.pink, Color.red, Color.white, Color.yellow
    };

    /* ------------------------------------------------
       Members
       ------------------------------------------------ */

	/* history of movements  */
	private ArrayList m_history;

    /* values to keep track of the Turtle's position and direction */
    private double m_xPos;
    private double m_yPos;
    private double m_angle;
    private double m_distance;

    /* the Turtle's range of movement */
    private int m_islandHeight;
    private int m_islandWidth;

    /* the colour that the Turtle is drawing in */
    private int m_drawColour;

    /* identifies whether the turtle is leaving a mark or not */
    private boolean m_penDraws;

    /* ------------------------------------------------
       Constructors
       ------------------------------------------------ */

	public Turtle(int width, int height) {
		m_history = new ArrayList(64);
		m_angle = 0.0;
		m_penDraws = false;
		setSize(width, height);
	}
	
	public void setSize(int width, int height) {
		m_islandHeight = height;
		m_islandWidth = width;
		goHome();
	}
	
    /**
     * Constructs a new Turtle object in the centre of the drawing area, facing north,
     * with its pen up and a pen colour of black.
     *
    public Turtle(Graphics g, int width, int height) {
        m_islandWidth = width;
        m_islandHeight = height;
        //m_graphics = g;
        m_angle = 0.0;
        m_penDraws = false;
        m_drawColour = 0;
        goHome();
    }
    */

	/* ------------------------------------------------
	   Turtle meta-controls
	   ------------------------------------------------ */

	/**
	 * Clears the Turtle's history. The next command will create a new TurtleStep
	 * in the history.
	 */
	public void clearHistory() {
		m_history.clear();
	}

    /* ------------------------------------------------
       Turtle controls
       ------------------------------------------------ */

    /**
     * Sends the Turtle object to the centre of the applet window.
     */
    public void goHome() {
        m_xPos = ((double)m_islandWidth) / 2.0;
        m_yPos = ((double)m_islandHeight) / 2.0;
        addStepToHistory();
    }

    /**
     * Moves the Turtle in the direction it is facing. The distance moved is the distance
     * moved in the previous move command.
     */
    public void move() {
        move(m_distance);
    }

    /**
     * Moves the Turtle in the direction it is facing.
     * @param distance
     *      The distance the turtle will move.
     */
    public void move(double distance) {

		// TODO - there seems to be a problem with precision

        double radians = Math.toRadians(m_angle);

        m_xPos += (distance * Math.sin(radians));
        m_yPos -= (distance * Math.cos(radians));
		m_distance = distance;
		addStepToHistory();
    }

    /**
     * Sets a new distance value for future <B>move</B>() calls.
     * @param distance
     *      The distance to be travelled by the Turtle.
     */
    public void newDistance(double distance) {
		m_distance = distance;
    }

    /**
     * Sets a new distance value for future <B>move</B>() calls to this Turtle.
     * @param percentage
     *      The ratio of the currently stored distance.
     */
    public void newDistanceRatio(double percentage) {
		m_distance *= percentage / 100.0;
    }

    /**
    * Sets a new direction for the Turtle to face. The angle of degrees to turn is
    * determined by the most recent <B>turn</B>(float turnAngle) call.
    */
    public void turn() {
        turn(m_angle);
    }

    /**
     * Sets a new direction for the Turtle to face.
     * @param turnAngle
     *      the angle of degrees to vary from the current direction that the Turtle is facing.
     */
    public void turn(double turnAngle) {
        m_angle += turnAngle;
        m_angle %= DEGREES_IN_CIRCLE;
    }

    /**
     * Sets a new vector for the Turtle.
     * @param angle
     *      the angle of the vector.
     */
    public void setDirection(double angle) {
        m_angle = angle % DEGREES_IN_CIRCLE;
    }

	// TODO - colour support
    /**
     * Clears the applet and sets the background colour.
     * @param colour
     *      the colour that the applet's background will be.
     */
    public void clear() {
    	addEraseStepToHistory();
    }

    /**
     * Tells the Turtle not to draw upon a move.
     */
    public void penUp() {
        m_penDraws = false;
    }

    /**
     * Tells the Turtle it should draw upon a move.
     */
    public void penDown() {
        m_penDraws = true;
    }

    /**
     * Changes the current drawing colour of the Turtle.
     * @param colour
     *      the colour to be used for drawing.
     *
    public void setDrawColour(int colour) {
        m_drawColour = colour % COLOUR_LIST.length;
        m_graphics.setColor(COLOUR_LIST[m_drawColour]);
    }
    */
    
    /**
     * Increments the colour by one.
     *
    public void incrementColour() {
        setDrawColour(++m_drawColour);
    }
    */
    
    /**
     * Creates a new TurtleStep and adds it to the TurtleHistory
     */
    private void addStepToHistory() {
    	TurtleStep ts = new TurtleStep(false, m_penDraws, Color.BLACK, m_xPos, m_yPos);
    	m_history.add(ts);
    }
    
    /**
     * Creates a new TurtleStep and adds it to the TurtleHistory. This step will
     * cause the screen to erase and will not draw anything, even if the turtle
     * has moved.
     */
    private void addEraseStepToHistory() {
    	TurtleStep ts = new TurtleStep(true, false, Color.BLACK, m_xPos, m_yPos);
    	m_history.add(ts);
    }
    
    public Iterator getHistory() {
    	return m_history.iterator();
    }
}
