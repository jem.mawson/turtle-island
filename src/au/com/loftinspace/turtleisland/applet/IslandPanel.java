/*
 * Created on 8/11/2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package au.com.loftinspace.turtleisland.applet;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Panel;
import java.util.Iterator;

import au.com.loftinspace.turtleisland.Turtle;
import au.com.loftinspace.turtleisland.TurtleStep;

/**
 * @author Jem
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class IslandPanel extends Panel {
	
	private Turtle m_turtle;
	
	public void paint(Graphics graphics) {
		drawTurtleHistory(graphics);
	}

	// TODO - refactor
	public void giveTurtle(Turtle turtle) {
		m_turtle = turtle;
		m_turtle.setSize(this.getWidth(), this.getHeight());
	}

	private void drawTurtleHistory(Graphics graphics) {
		TurtleStep step;
		boolean firststep = true;
		int x = 0;
		int y = 0;
		Iterator iter = m_turtle.getHistory();
		while (iter.hasNext()) {
			step = (TurtleStep)iter.next();
			
			int x2 = (int)Math.round(step.getXPos());
			int y2 = (int)Math.round(step.getYPos());
			
			if (firststep) {
				firststep = false;
				x = x2;
				y = y2;
				continue;
			}

			// if this step is an erase then erase!
			// TODO erase in different colours
			if (step.isCleanSlate()) {
				graphics.setColor(Color.WHITE);
				graphics.clearRect(0, 0, this.getWidth(), this.getHeight());
				continue;
			}
			
			// if this step declares a draw, then draw from last step 
			if (step.isPenDown()) {
				graphics.setColor(step.getColour());
				graphics.drawLine(x, y, x2, y2);
			}
			
			// record this step coordinates for the next step
			x = x2;
			y = y2;
		}
	}
}
