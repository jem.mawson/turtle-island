/*
 * Created on 7/11/2003
 */
package au.com.loftinspace.turtleisland.applet;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import au.com.loftinspace.turtleisland.Turtle;
import au.com.loftinspace.turtleisland.command.Instruction;
import au.com.loftinspace.turtleisland.parser.ScriptParser;
import au.com.loftinspace.turtleisland.parser.TurtleParseException;
import au.com.loftinspace.turtleisland.util.Messages;

/**
 * @author Jem
 */
public class TurtleIsland extends Applet implements ActionListener {

	// TODO remove this and make it a form input from HTML page (javascript?)
	private static final String SCRIPT = "loop(180) {" + 
		System.getProperty("line.separator") +
		"    move(3);" +
		System.getProperty("line.separator") +
		"    turn(2);" +
		System.getProperty("line.separator") +
		"}";

	// the Turtle who will be drawing stuff on his island
	private Turtle m_turtle;
	
	// the script parser will review the script.
	private ScriptParser m_parser;
	
	// the awt components
	private TextArea m_codeBox;
	private IslandPanel m_island;
	private Button m_runButton;

	public void init() {
		this.setBackground(Color.ORANGE);
		m_parser = new ScriptParser();
		m_codeBox = new TextArea(SCRIPT, 1, 30);
		m_island = new IslandPanel();
		m_runButton = new Button(Messages.getString("TurtleIsland.execute_script"));
		m_runButton.addActionListener(this);

		// create the layout
		this.setLayout(new BorderLayout());
		this.add(m_island, BorderLayout.CENTER);
		Panel rightSide = new Panel(new BorderLayout());
		rightSide.add(m_codeBox, BorderLayout.CENTER);
		rightSide.add(m_runButton, BorderLayout.SOUTH);
		this.add(rightSide, BorderLayout.EAST);

		m_turtle = new Turtle(getWidth(), getHeight());
	}

	public void start() {
		m_island.giveTurtle(m_turtle);
	}

	private void createTurtleHistory(String script) throws TurtleParseException {
		m_turtle.goHome();
		m_turtle.setDirection(0.0);
		m_turtle.penDown();
		m_turtle.clearHistory();
		Instruction instruction;
		instruction = m_parser.parse(script);
		if (instruction == null) {
			return;
		}
		instruction.setTurtle(m_turtle);
		instruction.execute();
	}
	

	/**
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(m_runButton)) {
			String script = m_codeBox.getText();
			try {
				createTurtleHistory(script);
			} catch(TurtleParseException tpe) {
				this.showStatus(tpe.getMessage());
			}
			m_island.repaint();
		}
	}

}
