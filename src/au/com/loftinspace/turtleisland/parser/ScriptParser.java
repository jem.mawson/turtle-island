package au.com.loftinspace.turtleisland.parser;

import java.text.StringCharacterIterator;

import au.com.loftinspace.turtleisland.command.BlockInstruction;
import au.com.loftinspace.turtleisland.command.Instruction;
import au.com.loftinspace.turtleisland.command.InstructionFactory;
import au.com.loftinspace.turtleisland.util.Messages;


public class ScriptParser {
    
    private CommandChainNode m_node = null;

    public ScriptParser() {}
    
	/**
	 * Parses a script (String) and converts it into a BlockCommand (Instruction)
	 * @param script
	 * 		the script to be parsed
	 * @return
	 * 		the BlockCommand representing the script
	 * @throws TurtleParseException
	 * 		if there are any errors in the script
	 */
    public Instruction parse(String script) throws TurtleParseException {

		m_node = null;
        int nesting = 0;
        int brackets = 0;
        StringBuffer command = new StringBuffer(32);
        String[] lines = script.split(System.getProperty("line.separator"));
        StringCharacterIterator sci = new StringCharacterIterator("");
        int chr = 0;
        for (int i = 0; i < lines.length; i++) {
            chr = 0;
            sci.setText(lines[i]);
            char thisChar = sci.first();
            while(thisChar != StringCharacterIterator.DONE) {
                chr++;
                switch(thisChar) {
                    case '}':
                        nesting--;
                        break;
                    case '(':
                        brackets++;
                        command.append(thisChar);
                        break;
                    case ')':
                        brackets--;
                        command.append(thisChar);
                        break;
                    case ' ':
                    case '\t':
                        break;
                    case '{': 
                        makeCommand(command.toString(), nesting, i+1, chr);
                        command.delete(0, command.length());
                        nesting++;
                        break;
                    case ';':
                        makeCommand(command.toString(), nesting, i+1, chr);
                        command.delete(0, command.length());
                        break;
                    default:
                        command.append(thisChar);
                }
                
                if (nesting < 0) {
                    throw new TurtleParseException("Braces do not match", i+1, chr);
                }
                if (brackets < 0) {
                    throw new TurtleParseException("Brackets do not match", i+1, chr);
                }
                
                thisChar = sci.next();
            }
        }
        if (nesting != 0) {
            throw new TurtleParseException("Braces do not match", lines.length, chr+1);
        }
        if (brackets != 0) {
            throw new TurtleParseException("Brackets do not match", lines.length, chr+1);
        }
        
        BlockInstruction bi = makeInstruction(m_node);
        return bi;
    }

    private void makeCommand(String command, int nesting, int line, int chr) throws TurtleParseException {
        
		CommandChainNode node = null;

		// if the command is loop, we can attempt to parse two commands
		// from the input String. Eg loop(8) move(10);
		if ((command.startsWith(Messages.getString("InstructionFactory.loop_5"))) &&
			(command.indexOf(')') != command.length() - 1)) {
				
			String firstPart = command.substring(0, command.indexOf(')') + 1);
			String secondPart = command.substring(command.indexOf(')') + 1).trim();

			node = new CommandChainNode(firstPart, nesting, line, 
				chr-command.length());
			CommandChainNode secondNode =
				new CommandChainNode(secondPart, nesting, line, 
					chr-command.length()+firstPart.length());
			
			node.add(secondNode);
		} else {
			node = new CommandChainNode(command.toString(), nesting, 
				line, chr-command.length());
		}

        if (m_node == null) {
            m_node = node;
        } else {
            m_node.add(node);
        }
    }
    
    /**
     * Converts a command chain into a BlockInstruction, or throws TurtleParseException if any
     * of the commands are not parsable.
     */
    private BlockInstruction makeInstruction(CommandChainNode node) 
    		throws TurtleParseException {
		
    	if (node == null) {
    		return null;
    	}

		BlockInstruction blockInstruction = new BlockInstruction();
    	Instruction priorInstruction = null;
    	
    	int nesting = node.getNesting();
    	while ((node != null) && (node.getNesting() >= nesting)) {
    		
    		Instruction instruction = null;
    		
    		if (node.getNesting() == nesting) {
    			instruction = InstructionFactory.create(node);
    			node = node.next();
    		} else {
    			instruction = makeInstruction(node);
    			node = node.nextBoss();
    		}
    		
			String instStr = "" + instruction;
    		if ((priorInstruction != null) && (priorInstruction.isComposite())) {
    			priorInstruction.add(instruction);
    		} else {
    			blockInstruction.add(instruction);
    		}
    		
    		priorInstruction = instruction;
    	}
    	
    	return blockInstruction;
	}
}