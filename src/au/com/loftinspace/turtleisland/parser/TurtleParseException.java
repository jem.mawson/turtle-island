package au.com.loftinspace.turtleisland.parser;

public class TurtleParseException extends Exception {

    String m_message;
    int m_line;
    int m_charPos;

    public TurtleParseException(String msg, int line, int charPos) {
        m_message = msg + ": line " + line + ", char " + charPos;
        m_line = line;
        m_charPos = charPos;
    }
    
    public TurtleParseException(String msg, int line, int charPos, Throwable cause) {
        super(cause);
        m_message = msg + ": line " + line + ", char " + charPos;
        m_line = line;
        m_charPos = charPos;
    }
    
    public String getMessage() {
        return m_message;
    }
    
    public int getLine() {
        return m_line;
    }
    
    public int getCharPos() {
        return m_charPos;
    }
}