package au.com.loftinspace.turtleisland.parser;

public class CommandChainNode {
    private int m_nesting;
    private int m_lineNumber;
    private int m_characterNumber;
    private String m_command;
    private Object[] m_parameters;
    private CommandChainNode m_next = null;
    
    public CommandChainNode(String command, int nesting, int line, int chr) 
    		throws TurtleParseException {
        
        m_nesting = nesting;
        m_lineNumber = line;
        m_characterNumber = chr;
		m_parameters = new Object[] {};
        
        // separate into command and parameters
        String twoParts[] = command.split("\\)|\\(");
        
        if (twoParts.length > 2) {
            throw new TurtleParseException("Command \"" + command + 
                    "\" has characters after the parameter list", line, chr);
        }
        
        // record command
        m_command = twoParts[0];

        // separate & record parameters
        String[] parameters = new String[0];
        if (twoParts.length == 2) {
            parameters = twoParts[1].split(",");
        }
        
        // for each parameter, convert it to the correct type.
        m_parameters = new Object[parameters.length];
        for (int i = 0; i < parameters.length; i++) {
        	String thisParam = parameters[i];
        	
			// if it starts and ends with double quotes, it's a string
			if (thisParam.startsWith("\"") && thisParam.endsWith("\"")) {
				m_parameters[i] = thisParam.substring(1, thisParam.length() - 1);
				continue;
			}
			
			// if it contains a '.' then parse as a float
			if (thisParam.indexOf('.') != -1) {
				m_parameters[i] = new Float(thisParam); 
				continue;
			}
			
			// otherwise, try to parse as an int
			m_parameters[i] = new Integer(thisParam);
        }
    }
    
    public void add(CommandChainNode ccn) {
        if (m_next == null) {
            m_next = ccn;
            return;
        }
        m_next.add(ccn);
    }
    
    public CommandChainNode next() {
        return m_next;
    }
    
	/**
	 * Returns the next CommandChainNode that has a nesting level above 
	 * this instance. Can return null.
	 */
	public CommandChainNode nextBoss() {
		CommandChainNode nextBossNode = this.next();
		while ((nextBossNode != null) && 
			(nextBossNode.getNesting() >= this.getNesting())) {

				nextBossNode = nextBossNode.next();
		}
		return nextBossNode;
	}
    
    public int getNesting() {
        return m_nesting;
    }
    
    public String getCommandString() {
    	return m_command;
    }
    
    public Object[] getCommandParameters() {
    	return m_parameters;
    }
    
    public String toString() {
        StringBuffer buff = new StringBuffer(64);
        for (int i = 0; i < m_nesting; i++) {
            buff.append("    ");
        }
        buff.append(m_command);
        buff.append("(");
        if (m_parameters != null) {
            for (int i = 0; i < m_parameters.length; i++) {
                buff.append(m_parameters[i]);
                if (i != m_parameters.length-1) {
                    buff.append(", ");
                }
            }
        }
        buff.append(")");
        return buff.toString();
    }
    
    public String toChainedString() {
        StringBuffer buff = new StringBuffer(toString());
        buff.append(System.getProperty("line.separator"));
        if (m_next != null) {
            buff.append(m_next.toChainedString());
        }
        return buff.toString();
    }
    
	/**
	 * @return
	 */
	public int getCharacterNumber() {
		return m_characterNumber;
	}

	/**
	 * @return
	 */
	public int getLineNumber() {
		return m_lineNumber;
	}

}