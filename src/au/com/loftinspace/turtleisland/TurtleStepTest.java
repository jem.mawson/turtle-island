/*
 * Created on 4/11/2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package au.com.loftinspace.turtleisland;

import java.util.Iterator;

import org.apache.log4j.Logger;

import au.com.loftinspace.turtleisland.command.Instruction;
import au.com.loftinspace.turtleisland.parser.ScriptParser;
import au.com.loftinspace.turtleisland.parser.TurtleParseException;
import au.com.loftinspace.utility.FileLoader;

/**
 * @author Jem
 */
public class TurtleStepTest extends BaseTurtleIslandUnitTestCase {

	private static Logger logger = Logger.getLogger(TurtleStepTest.class);

  	public void testSquare() throws Exception {
	  	String script = FileLoader.loadTextFile("test/input/au.com.loftinspace.turtleisland.TurtleStepTest.testSquare.txt");
	  	String expectedOutcome = FileLoader.loadTextFile("test/output/au.com.loftinspace.turtleisland.TurtleStepTest.testSquare.txt");
		performTurtleStepTest(script, expectedOutcome);
  	}

	public void testOctagon() throws Exception {
		String script = FileLoader.loadTextFile("test/input/au.com.loftinspace.turtleisland.TurtleStepTest.testOctagon.txt");
		String expectedOutcome = FileLoader.loadTextFile("test/output/au.com.loftinspace.turtleisland.TurtleStepTest.testOctagon.txt");
		performTurtleStepTest(script, expectedOutcome);
	}

	public void testNoBrace() throws Exception {
		String script = FileLoader.loadTextFile("test/input/au.com.loftinspace.turtleisland.TurtleStepTest.testNoBrace.txt");
		String expectedOutcome = FileLoader.loadTextFile("test/output/au.com.loftinspace.turtleisland.TurtleStepTest.testNoBrace.txt");
		performTurtleStepTest(script, expectedOutcome);
	}

	public void testMeaninglessBrace() throws Exception {
		String script = FileLoader.loadTextFile("test/input/au.com.loftinspace.turtleisland.TurtleStepTest.testMeaninglessBrace.txt");
		String expectedOutcome = FileLoader.loadTextFile("test/output/au.com.loftinspace.turtleisland.TurtleStepTest.testMeaninglessBrace.txt");
		performTurtleStepTest(script, expectedOutcome);
	}

	public void testInnerLoop() throws Exception {
		String script = FileLoader.loadTextFile("test/input/au.com.loftinspace.turtleisland.TurtleStepTest.testInnerLoop.txt");
		String expectedOutcome = FileLoader.loadTextFile("test/output/au.com.loftinspace.turtleisland.TurtleStepTest.testInnerLoop.txt");
		performTurtleStepTest(script, expectedOutcome);
	}
	
	public void testUnbalancedBrackets() throws Exception {
		String script = FileLoader.loadTextFile("test/input/au.com.loftinspace.turtleisland.TurtleStepTest.testUnbalancedBrackets.txt");
		try {
			performTurtleStepTest(script, "");
			fail("Expected TurtleParseException");
		}
		catch(TurtleParseException e) {
		}
	}

	private void performTurtleStepTest(String script, String expectedOutcome) throws TurtleParseException {
		logger.debug("Instruction:");
		logger.debug(script);
		
		String[] steps = expectedOutcome.split("\n");
		
		ScriptParser sp = new ScriptParser();
		Instruction instruction = sp.parse(script);
		Turtle turtle = new Turtle(600, 400);
		instruction.setTurtle(turtle);
		instruction.execute();
		
		logger.debug("!!"+instruction);
		
		Iterator iter = turtle.getHistory();
		int stepCount = 0;
		while (iter.hasNext()) {
			TurtleStep ts = (TurtleStep)iter.next();
			String[] step = steps[stepCount].split(",");
			logger.debug(ts);
			assertEquals(3, step.length); 
    	 	assertEquals(step[0].trim(), (ts.isPenDown() ? "draw" : "move"));
			assertEquals(Long.valueOf(step[1].trim()), new Long(Math.round(ts.getXPos())));
			assertEquals(Long.valueOf(step[2].trim()), new Long(Math.round(ts.getYPos())));
			stepCount++;
		}
		assertEquals(stepCount, steps.length);
    }
}

