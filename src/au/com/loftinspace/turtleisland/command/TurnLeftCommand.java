package au.com.loftinspace.turtleisland.command;

public class TurnLeftCommand extends TurnCommand {

    public TurnLeftCommand() {
        super(-90.0);
    }
}