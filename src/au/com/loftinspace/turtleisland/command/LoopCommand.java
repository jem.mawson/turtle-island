package au.com.loftinspace.turtleisland.command;

import au.com.loftinspace.turtleisland.Turtle;

public class LoopCommand extends Instruction {

    private int m_iterations = 0;
    private Instruction m_instruction;

	public LoopCommand(int iterations) {
		m_iterations = (iterations > 0) ? iterations : 0;
	}

	public void setTurtle(Turtle turtle) {
		super.setTurtle(turtle);
		if (m_instruction != null) {
			m_instruction.setTurtle(turtle);
		}
	}

	/**
	 * Sets the instruction to be iterated.
	 * @param instruction
	 * 		the instruction to be encapsulated
	 */
	public void add(Instruction instruction) {
		m_instruction = instruction;
	}

	/**
	 * Whether the instruction expects to encapsulate other Instruction(s)
	 */
	public boolean isComposite() {
		return true;
	}

    public void execute() {
        if (m_instruction != null) {
            for (int i = 0; i < m_iterations; i++) {
                m_instruction.execute();
            }
        }
    }
    
    public String toString() {
    	return super.toString() + System.getProperty("line.separator") + m_instruction;
    }

}