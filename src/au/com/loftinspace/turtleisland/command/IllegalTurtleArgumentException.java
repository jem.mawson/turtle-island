package au.com.loftinspace.turtleisland.command;

public class IllegalTurtleArgumentException extends Exception {

    public IllegalTurtleArgumentException(String msg) {
        super(msg);
    }

}