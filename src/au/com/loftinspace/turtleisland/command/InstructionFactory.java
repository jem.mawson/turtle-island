package au.com.loftinspace.turtleisland.command;

import au.com.loftinspace.turtleisland.parser.CommandChainNode;
import au.com.loftinspace.turtleisland.parser.TurtleParseException;
import au.com.loftinspace.turtleisland.util.*;

public class InstructionFactory {
    
    private static final Class[] NO_PARAMS = new Class[] {};
    private static final Class[] SINGLE_INT = new Class[] {Integer.class};
    private static final Class[] SINGLE_FLOAT = new Class[] {Float.class};
    private static final Class[] SINGLE_STRING = new Class[] {String.class};
    
    public static Instruction create(CommandChainNode ccn) throws TurtleParseException {
        
        // TODO - reference commands to classes in resource bundle and dynamic instantiate?
        
        String command = ccn.getCommandString();
        Object[] parameters = ccn.getCommandParameters();
        
        // empty statement
        if (command.equals("")) { //$NON-NLS-1$
        	return new EmptyStatement();
        }
        
        // NORTH command
        if (command.equalsIgnoreCase(Messages.getString("InstructionFactory.north_2"))) { //$NON-NLS-1$
			if (checkParameters(ccn, NO_PARAMS)) {
				return new FaceNorthCommand();
			}
        }
        
        // HOME command
        if (command.equalsIgnoreCase(Messages.getString("InstructionFactory.home_3"))) { //$NON-NLS-1$
        	if (checkParameters(ccn, NO_PARAMS)) { 
        		return new HomeCommand();
        	}
        }
        
        // ERASE command
        if (command.equalsIgnoreCase(Messages.getString("InstructionFactory.erase_4"))) { //$NON-NLS-1$
        	if (checkParameters(ccn, NO_PARAMS)) {
        		return new EraseCommand();
        	}
        }

		// LOOP command
		if (command.equalsIgnoreCase(Messages.getString("InstructionFactory.loop_5"))) { //$NON-NLS-1$
			if (checkParameters(ccn, SINGLE_INT)) {
				return new LoopCommand(((Integer)parameters[0]).intValue());
			}
		}
		
		// MOVE command
		if (command.equalsIgnoreCase(Messages.getString("InstructionFactory.move_6"))) { //$NON-NLS-1$
			if (checkParameters(ccn, NO_PARAMS)) {
				return new MoveCommand();
			} else if (checkParameters(ccn, SINGLE_INT)){
				return new MoveCommand(((Integer)parameters[0]).intValue());
			}
		}
		
		// PENDOWN command
		if (command.equalsIgnoreCase(Messages.getString("InstructionFactory.penDown_7"))) { //$NON-NLS-1$
			if (checkParameters(ccn, NO_PARAMS)) {
				return new PenDownCommand();
			}
		}
		
		// PENUP command
		if (command.equalsIgnoreCase(Messages.getString("InstructionFactory.penUp_8"))) { //$NON-NLS-1$
			if (checkParameters(ccn, NO_PARAMS)) {
				return new PenUpCommand();
			}
		}
		
		// RATIO command
		if (command.equalsIgnoreCase(Messages.getString("InstructionFactory.ratio_9"))) { //$NON-NLS-1$
			if (checkParameters(ccn, SINGLE_FLOAT)) {
				return new RatioDistanceCommand(((Float)parameters[0]).floatValue());
			}
			if (checkParameters(ccn, SINGLE_INT)) {
				return new TurnCommand(((Integer)parameters[0]).floatValue());
			}
		}
		
		// DISTANCE command
		if (command.equalsIgnoreCase(Messages.getString("InstructionFactory.distance_10"))) { //$NON-NLS-1$
			if (checkParameters(ccn, SINGLE_FLOAT)) {
				return new SetDistanceCommand(((Float)parameters[0]).floatValue());
			}
			if (checkParameters(ccn, SINGLE_INT)) {
				return new TurnCommand(((Integer)parameters[0]).floatValue());
			}
		}
		
		// ANGLE command
		if (command.equalsIgnoreCase(Messages.getString("InstructionFactory.angle_11"))) { //$NON-NLS-1$
			if (checkParameters(ccn, SINGLE_FLOAT)) {
				return new SetVectorCommand(((Float)parameters[0]).floatValue());
			}
			if (checkParameters(ccn, SINGLE_INT)) {
				return new TurnCommand(((Integer)parameters[0]).floatValue());
			}
		}
		
		// TURN command
		if (command.equalsIgnoreCase(Messages.getString("InstructionFactory.turn_12"))) { //$NON-NLS-1$
			if (checkParameters(ccn, SINGLE_FLOAT)) {
				return new TurnCommand(((Float)parameters[0]).floatValue());
			}
			if (checkParameters(ccn, SINGLE_INT)) {
				return new TurnCommand(((Integer)parameters[0]).floatValue());
			}
		}
		
		// LEFT command
		if (command.equalsIgnoreCase(Messages.getString("InstructionFactory.left_13"))) { //$NON-NLS-1$
			if (checkParameters(ccn, NO_PARAMS)) {
				return new TurnLeftCommand();
			}
		}
		
		// RIGHT command
		if (command.equalsIgnoreCase(Messages.getString("InstructionFactory.right_14"))) { //$NON-NLS-1$
			if (checkParameters(ccn, NO_PARAMS)) {
				return new TurnRightCommand();
			}
		}
		
		// if no command has been passed, then throw an exception
		throwParameterException(ccn);
		return new EmptyStatement(); // will not reach here, for compiler only
    }
    
	/**
	 * Whether the CommandChainNode has the same parameters as the definition 
	 * provided in the int array 
	 * 
	 * @param ccn
	 * 		the command being asserted upon 
	 * @param params
	 * 		the parameter types that the command should have
	 */
    private static boolean checkParameters(CommandChainNode ccn, Class[] params) {

    	// TODO - protect against the npe

    	Object[] actualParams = ccn.getCommandParameters();
    	
		// if number of params differs from number of params sought
		if (actualParams.length != params.length) {
			return false;
		}

		// ensure each object is of the correct class
		for (int i = 0; i < actualParams.length; i++) {
			if (!(actualParams[i].getClass().equals(params[i]))) {
				return false;
			}
		}
    		
    	return true;	
	}
    
	/**
	 * Helper method to throw a TurtleParameterException.
	 * @param ccn
	 * 		details for the exception message
	 * @throws
	 * 		TurtleParameterException - always.
	 */
	private static void throwParameterException(CommandChainNode ccn)
			throws TurtleParseException {
				
		StringBuffer msg = new StringBuffer(64);
		msg.append("Command "); //$NON-NLS-1$
		msg.append(ccn.getCommandString());
		msg.append(" does not have the correct parameters"); //$NON-NLS-1$
		throw new TurtleParseException(msg.toString(), ccn.getLineNumber(),
			ccn.getCharacterNumber());
	}

}