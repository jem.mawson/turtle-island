package au.com.loftinspace.turtleisland.command;

public class TurnCommand extends Instruction {

    private double m_angle = 0.0;
    private boolean m_usePresetAngle;

    public TurnCommand(double angle) {
        angle %= 360.0;
        m_angle = angle;
        m_usePresetAngle = false;
    }
    
    public TurnCommand() {
        m_usePresetAngle = true;
    }

    public void execute() {
        if (m_usePresetAngle) {
            m_turtle.turn();
        } else {
            m_turtle.turn(m_angle);
        }
    }

}