package au.com.loftinspace.turtleisland.command;

public class RatioDistanceCommand extends Instruction {

    private double m_ratio;

    public RatioDistanceCommand(double ratio) {
        m_ratio = ratio;
    }
    
    public void execute() {
        m_turtle.newDistanceRatio(m_ratio);
    }
}