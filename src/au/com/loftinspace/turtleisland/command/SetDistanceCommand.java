package au.com.loftinspace.turtleisland.command;

public class SetDistanceCommand extends Instruction {

    private double m_distance;

    public SetDistanceCommand(double distance) {
        m_distance = distance;
    }
    
    public void execute() {
        m_turtle.newDistance(m_distance);
    }
}