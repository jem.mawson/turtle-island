package au.com.loftinspace.turtleisland.command;

public class TurnRightCommand extends TurnCommand {

    public TurnRightCommand() {
        super(90.0);
    }
}