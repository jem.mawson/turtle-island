package au.com.loftinspace.turtleisland.command;

// TODO - implement different colour support, as parameter

public class EraseCommand extends Instruction {

    public EraseCommand() {
    }

    public void execute() {
        m_turtle.clear();
    }
}