package au.com.loftinspace.turtleisland.command;

public class PenUpCommand extends Instruction {

    public PenUpCommand() {
    }

    public void execute() {
        m_turtle.penUp();
    }

}