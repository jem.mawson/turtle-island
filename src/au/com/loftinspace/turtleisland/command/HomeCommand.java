package au.com.loftinspace.turtleisland.command;

public class HomeCommand extends Instruction {

    public HomeCommand() {}

    public void execute() {
        m_turtle.goHome();
    }

}