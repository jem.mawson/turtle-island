package au.com.loftinspace.turtleisland.command;

import au.com.loftinspace.turtleisland.Turtle;

public abstract class Instruction {
    
    protected Turtle m_turtle;
    
    public Instruction() {}
    
	public void setTurtle(Turtle turtle) {
		m_turtle = turtle;
	}

	public String toString() {
		return this.getClass().getName();
	}
	
	/**
	 * Encapsulated an instruction.
	 * @param instruction
	 * 		the instruction to be encapsulated
	 */
	public void add(Instruction instruction) {
		// nothing to do in most subclasses
	}
	
	/**
	 * Whether the instruction expects to encapsulate other Instruction(s)
	 */
	public boolean isComposite() {
		return false;
	}

    public abstract void execute();

}