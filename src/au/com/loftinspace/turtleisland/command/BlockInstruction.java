package au.com.loftinspace.turtleisland.command;

import java.util.ArrayList;
import java.util.Iterator;

import au.com.loftinspace.turtleisland.Turtle;

public class BlockInstruction extends Instruction {
    
    private static final int LIST_INIT_SIZE = 16;
    private ArrayList m_instructions;
    
    public BlockInstruction() {
        m_instructions = new ArrayList(LIST_INIT_SIZE);
    }

    public void add(Instruction instruction) {
        m_instructions.add(instruction);
    }
    
	/**
	 * Whether the instruction expects to encapsulate other Instruction(s)
	 */
	public boolean isComposite() {
		return false;
	}

    public void execute() {
        Iterator iter = m_instructions.iterator();
        Instruction instruction;
        while (iter.hasNext()) {
            instruction = (Instruction) iter.next();
            instruction.execute();
        }
    }
    
	/**
	 * Sets the Turtle reference in each member instruction.
	 */
    public void setTurtle(Turtle t) {
    	Iterator iter = m_instructions.iterator();
    	Instruction instruction;
    	while (iter.hasNext()) {
    		instruction = (Instruction)iter.next();
    		instruction.setTurtle(t);
	    }
    }

	public String toString() {
		StringBuffer buff = new StringBuffer(64);
		buff.append("{");
		buff.append(System.getProperty("line.separator"));
		Iterator iter = m_instructions.iterator();
		while (iter.hasNext()) {
			buff.append(((Instruction)iter.next()).toString());
			buff.append(System.getProperty("line.separator"));
		}
		buff.append("}");
		return buff.toString();
	}
}