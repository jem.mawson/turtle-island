package au.com.loftinspace.turtleisland.command;

public class FaceNorthCommand extends SetVectorCommand {

    public FaceNorthCommand() {
        super(0.0);
    }
}