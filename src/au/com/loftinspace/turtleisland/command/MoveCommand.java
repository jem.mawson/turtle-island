package au.com.loftinspace.turtleisland.command;

public class MoveCommand extends Instruction {

    private double m_distance = 0.0;
    private boolean m_sameDistanceAsBefore;
    
    public MoveCommand() {
        m_sameDistanceAsBefore = true;
    }

    public MoveCommand(double distance) {
        m_distance = distance;
        m_sameDistanceAsBefore = false;
    }

    public void execute() {
        if (m_sameDistanceAsBefore) {
            m_turtle.move();
        } else {
            m_turtle.move(m_distance);
        }
    }

}