package au.com.loftinspace.turtleisland;

import java.util.Iterator;

import junit.framework.TestCase;

/**
 * @author Jem
 */
public class TurtleTest extends TestCase {
	
	public void testTurtlePrecision() throws Exception {
		Turtle turtle = new Turtle(600, 400);
		turtle.setDirection(45.0);
		turtle.move(50.0);
		Iterator iter = turtle.getHistory();
		TurtleStep ts = (TurtleStep) iter.next();
	    assertNotNull(ts);
	    assertTrue(ts.getXPos() == 300.0);
	    assertTrue(ts.getYPos() == 200.0);
		ts = (TurtleStep) iter.next();
		assertNotNull(ts);
		double distance = Math.sqrt((50.0 * 50.0) / 2.0);
		assertTrue(ts.getXPos() == 300.0 + distance);
		assertTrue(ts.getYPos() == 200.0 - distance);
	}
	
	public void testTurtlePrecisionOverManyTurns() throws Exception {
		Turtle turtle = new Turtle(600, 400);
		for (double i = 0.0; i < 360.0; i += 0.5) {
			turtle.setDirection(i);
			turtle.move(3);
		}
		TurtleStep ts = null;
		Iterator iter = turtle.getHistory();
		while(iter.hasNext()) {
			ts = (TurtleStep) iter.next();
		}
		assertEquals(new Long(300), new Long(Math.round(ts.getXPos())));
		assertEquals(new Long(200), new Long(Math.round(ts.getYPos())));
	}

}
