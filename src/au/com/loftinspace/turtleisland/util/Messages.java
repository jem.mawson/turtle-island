/*
 * Created on 6/11/2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package au.com.loftinspace.turtleisland.util;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author Jem
 */
public class Messages {

	private static final String BUNDLE_NAME = 
		"au.com.loftinspace.turtleisland.util.messages";

	private static final ResourceBundle RESOURCE_BUNDLE =
		ResourceBundle.getBundle(BUNDLE_NAME);

	/**
	 * Cannot be instantiated.
	 */
	private Messages() {}

	/**
	 * @param key
	 * 		the key to be sought
	 * @return
	 * 		the value of the key, or an error message starting and ending with '!'
	 */
	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
