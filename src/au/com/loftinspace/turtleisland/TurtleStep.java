/*
 * Created on 6/11/2003
 */
package au.com.loftinspace.turtleisland;

import java.awt.Color;

/**
 * A step of the Turtle. Records x & y positions, along with drawstatus and colour.
 * @author Jem
 */
public class TurtleStep {

	private boolean m_erase;
	private boolean m_draw;
	private Color m_color;
	private double m_xPos;
	private double m_yPos;
	
	public TurtleStep(boolean erase, boolean draw, Color color, 
			double xPos, double yPos) {
		
		m_erase = erase;
		m_draw = draw;
		m_color = color;
		m_xPos = xPos;
		m_yPos = yPos;
	}
	
	public boolean isCleanSlate() {
		return m_erase;
	}

	public boolean isPenDown() {
		return m_draw;
	}
	
	public Color getColour() {
		return m_color;
	}
	
	public double getXPos() {
		return m_xPos;
	}
	
	public double getYPos() {
		return m_yPos;
	}
	
	public String toString() {
		if (m_erase) {
			return "erase screen";
		}
		StringBuffer buff = new StringBuffer(64);
		buff.append(m_draw ? "draw to " : "move to ");
		buff.append(m_xPos);
		buff.append(", ");
		buff.append(m_yPos);
		return buff.toString();
	}
}
